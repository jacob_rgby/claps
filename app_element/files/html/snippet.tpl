<!--

Global styling for cloned clap nodes / sidebar

 !-->
<style>

/*
button {
    background: none;
    border: none;
    padding: 0;
    font: inherit;
    cursor: pointer;
    outline: none;
}

.clap-circle {
    width: 60px;
    height: 60px;
    border: 1px solid rgba(0, 0, 0, .15);
    border-radius: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    transition: 0.4s border-color;
}

@keyframes doClap {
    0% {
        transform: scale(1);
    }
    50% {
        transform: scale(1.2);
    }
    100% {
        transform: scale(1);
    }
}


.clap-circle-animated {
    animation: doClap 0.7s ease-in; 
}

.clap-count {
    height: 20px;
    margin-bottom: 3px;
    color: rgba(0, 0, 0, .54);
    background: rgba(0, 0, 0, 0);
    font-size: 13px;
    text-decoration: none;
    cursor: pointer;
}

.clap-circle:hover {
    border-color: cornflowerblue;
}

.clap-btn {
    width: 60%;
    fill: cornflowerblue;
}

.clap svg {
    width: 100%;
}

.clap-wrapper {
    text-align: center;
    width: 60px;
}*/



button {
    background: none;
    border: none;
    padding: 0;
    font: inherit;
    cursor: pointer;
    outline: none;
}

.clap-circle {
    width: 60px;
    height: 60px;
    border: 1px solid rgba(0, 0, 0, .15);
    border-radius: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    transition: 0.4s border-color;
}

@keyframes doClap {
    0% {
        transform: scale(1.3);
    }
    100% {
        transform: scale(1);
    }
}


.clap-circle-animated {
    animation: doClap 0.3s ease-out; 
}

.clap-count {
    height: 20px;
    margin-bottom: 6px;
    /* position: relative; */
    color: rgba(0, 0, 0, .54);
    /*color: rgb(189, 195, 199);*/
    background: rgba(0, 0, 0, 0);
    font-size: 13px;
    text-decoration: none;
    cursor: pointer;
}

.clap-circle:hover {
    border-color: cornflowerblue;
}

.clap-btn {
    width: 60%;
    fill: cornflowerblue;
}

.clap svg {
    width: 100%;
}

.clap-wrapper {
    font-family: "Open Sans";
    text-align: center;
    width: 60px;
}


body {
      font-family: medium-content-sans-serif-font,-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Open Sans","Helvetica Neue",sans-serif;
    letter-spacing: 0;
    font-weight: 400;
    font-style: normal;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -moz-font-feature-settings: "liga" on;
    color: rgba(0,0,0,.84);
    font-size: 20px;
    line-height: 1.4;
}

.clap-sidebar {
  visibility: hidden;
  opacity: 0;
  -webkit-transition: visibility 0.3s linear 0s,opacity .3s 0s;
  transition: visibility 0.3s linear 0s,opacity .3s 0s;
  
  position: fixed;
  top: 200px;
  left: 60px;
}
</style>